
-- Fichier ou l'on test des  idees de fonctions analytiques sans utitliser de fonction analytique 

SELECT coalesce(my_tw.mot,c.mot,tw.mot),SUM(coalesce(tw.count,0)  + coalesce(c.count,0) * 1) FROM 
MOT1 my_tw 
FULL JOIN MOT2 c ON my_tw.mot=c.mot
FULL JOIN (SELECT mot , count FROM MOT hg ORDER BY hg.count FETCH FIRST 10 ROWS ONLY) tw
ON my_tw.mot =tw.mot 
GROUP BY coalesce(my_tw.mot,c.mot,tw.mot)
HAVING SUM(coalesce( my_tw.count,c.count,0)) >0 OR  SUM(coalesce(tw.count,0) + coalesce(c.count,0) * 1 ) > 5 ;   



SELECT tw.mot ,SUM(tw.count) FROM 
MOT1 my_tw 
FULL JOIN MOT2 c ON my_tw.mot=c.mot
FULL JOIN (SELECT mot , count FROM MOT hg ORDER BY hg.count FETCH FIRST 10 ROWS ONLY) tw
ON my_tw.mot =tw.mot 
GROUP BY tw.mot; 
 
SELECT tw.mot,SUM(tw.count) FROM 
MOT1 my_tw 
FULL JOIN MOT2 c ON my_tw.mot=c.mot
FULL JOIN (SELECT mot , count FROM MOT hg ORDER BY hg.count FETCH FIRST 10 ROWS ONLY) tw
ON my_tw.mot =tw.mot 
GROUP BY tw.mot; 


CREATE TABLE MOT1 ( mot VARCHAR2(100) , count NUMBER) ;


INSERT INTO MOT1 VALUES ( 'a' , 10 ) ;
INSERT INTO MOT1 VALUES ( 'b' , 11 ) ;
INSERT INTO MOT1 VALUES ( 'c' , 12 ) ;
INSERT INTO MOT1 VALUES ( 'd' , 13 ) ;
INSERT INTO MOT1 VALUES ( 'e' , 14 ) ;


CREATE TABLE MOT2 ( mot VARCHAR2(100) , count NUMBER) ;

INSERT INTO MOT2 VALUES ( 'z' , 2 ) ;
INSERT INTO MOT2 VALUES ( 'd' , 20 ) ;
INSERT INTO MOT2 VALUES ( 'e' , 21 ) ;
INSERT INTO MOT2 VALUES ( 'f' , 22 ) ;
INSERT INTO MOT2 VALUES ( 'g' , 23 ) ;

CREATE TABLE MOT3 ( mot VARCHAR2(100) , count NUMBER) ;


INSERT INTO MOT3 VALUES ( 'g' , 30 ) ;
INSERT INTO MOT3 VALUES ( 'h' , 31 ) ;
INSERT INTO MOT3 VALUES ( 'i' , 32 ) ;


CREATE TABLE MOT ( mot VARCHAR2(100) , count NUMBER) ;

INSERT INTO MOT VALUES ( 'a' , 0 ) ;
INSERT INTO MOT VALUES ( 'b' , 1 ) ;
INSERT INTO MOT VALUES ( 'c' , 2 ) ;
INSERT INTO MOT VALUES ( 'd' , 3 ) ;
INSERT INTO MOT VALUES ( 'e' , 4 ) ;
INSERT INTO MOT VALUES ( 'f' , 5 ) ;
INSERT INTO MOT VALUES ( 'g' , 6 ) ;
INSERT INTO MOT VALUES ( 'h' , 7 ) ;
INSERT INTO MOT VALUES ( 'i' , 8 ) ;


INSERT INTO MOT VALUES ( 'c' , 0 ) ;
INSERT INTO MOT VALUES ( 'd' , 1 ) ;
INSERT INTO MOT VALUES ( 'e' , 2 ) ;
INSERT INTO MOT VALUES ( 'f' , 3 ) ;


DROP TABLE MOT ;
DROP TABLE MOT1;
DROP TABLE MOT2;
DROP TABLE MOT3;



SELECT *  FROM MOT1 my_tw LEFT JOIN MOT hg ON my_tw.mot = hg.mot ;




